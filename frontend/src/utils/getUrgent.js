export default (payload) => {
  /*
   * We change the value of the furnised
   */
  let urgent = {
    false: 'is_not_urgent',
    true: 'is_urgent',
    'default': ''
  }
  return (urgent[payload] || urgent['default'])
}

export default (payload) => {
  /*
   * We change the value of the errors messages
   */
  let $form = document.querySelector('form')
  for (let i = 0; i < $form.querySelectorAll('div.form-group').length; i++) {
    let item = $form.querySelectorAll('div.form-group')[i]
    item.classList.remove('has-error')
    item.querySelector('span.error').innerHTML = ''
  }

  for (let i = 0; i < payload.error.messageList.length; i++) {
    let item = payload.error.messageList[i]
    let $controlGroup = document.getElementById(item.param.toString().replace(/\./g, ''))
    if ($controlGroup != null) {
      $controlGroup.classList.add('has-error')
      $controlGroup.querySelector('span.error').innerHTML = item.msg
    }
  }
}

/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

export default [
  // Home
  {
    path: '/',
    name: 'home.index',
    component: () => import('@/pages/Home/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: false
    }
  },

  // Term of use
  {
    path: '/term-of-use/',
    name: 'termofuse',
    component: () => import('@/pages/TermOfUse/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true
    }
  },
  // Privacy of policy
  {
    path: '/privacy-policy/',
    name: 'privacypolicy',
    component: () => import('@/pages/TermOfUse/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true
    }
  },

  // About us
  {
    path: '/about-us/',
    name: 'aboutus.index',
    component: () => import('@/pages/About/Index'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone concept
  {
    path: '/vone-concept/',
    name: 'voneconcept.index',
    component: () => import('@/pages/Concept/Index'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone advantage
  {
    path: '/vone-advantage/',
    name: 'voneadvantage.index',
    component: () => import('@/pages/Advantage/Index'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone relationship
  {
    path: '/vone-relationship/',
    name: 'vonerelationship.index',
    component: () => import('@/pages/Relationship/Index.vue'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Vone relationship
  {
    path: '/blog/',
    name: 'blog.index',
    component: () => import('@/pages/Blog/Index/Index'),
    // If the user needs to be authenticated to view this page.
    meta: {
      auth: false
    }
  },

  // Blog Show
  {
    path: '/blog/:id(\\d+)',
    name: 'blog.show',
    component: () => import('@/pages/Blog/Show/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true
    }
  },

  // Property
  {
    path: '/property/',
    component: () => import('@/pages/Property/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Property'
    },
    children: [
      {
        path: 'list',
        name: 'property.list',
        component: () => import('@/pages/Property/_List/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'List'
        }
      },
      {
        path: 'overview',
        name: 'property.overview',
        component: () => import('@/pages/Property/_Overview/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'Overview'
        }
      },
      // Profile new rent
      {
        path: 'new',
        name: 'property.new',
        component: () => import('@/pages/Property/_New/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'New'
        }
      },
      {
        path: ':id(\\d+)',
        component: () => import('@/pages/Property/_Show/Index'),
        meta: {
          auth: true,
          breadcrumb: 'Show'
        },
        children: [
          { path: '', name: 'property.show.overview', component: () => import('@/pages/Property/_Show/_Overview/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
          { path: 'statistics', name: 'property.show.statistics', component: () => import('@/pages/Property/_Show/_Statistics/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
          { path: 'proposition', name: 'property.show.propositions', component: () => import('@/pages/Property/_Show/_Propositions/Index'), meta: { auth: true, breadcrumb: 'Propositions' } },
          { path: 'settings', name: 'property.show.settings', component: () => import('@/pages/Property/_Show/_Settings/Index'), meta: { auth: true, breadcrumb: 'Settings' } },
          { path: 'edit', name: 'property.edit', component: () => import('@/pages/Property/_Edit/Index'), meta: { auth: true, breadcrumb: 'Property edit' } }
        ]
      }
    ]
  },

  // Notifications
  {
    path: '/notifications/',
    name: 'notification.index',
    component: () => import('@/pages/Notification/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
      breadcrumb: 'Notifications'
    }
  },

  // Buy
  {
    path: '/buy/',
    component: () => import('@/pages/Buy/Index/Index'),
    name: 'buy.index',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Buying proposal'
    }
  },

  // Buy list
  {
    path: '/buy/list',
    name: 'buy.list',
    component: () => import('@/pages/Buy/List/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Buying proposal List'
    }
  },
  // Buy New
  {
    path: '/buy/new',
    name: 'buy.new',
    component: () => import('@/pages/Buy/New/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Buying proposal New'
    }
  },

  {
    path: '/buy/overview',
    name: 'buy.overview',
    component: () => import('@/pages/Buy/Overview/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Overview'
    }
  },
  {
    path: '/buy/:id(\\d+)/edit',
    name: 'buy.edit',
    component: () => import('@/pages/Buy/Edit/Index'),
    meta: {
      auth: true,
      breadcrumb: 'Show'
    }
  },
  {
    path: '/buy/:id(\\d+)',
    component: () => import('@/pages/Buy/Show/Index'),
    meta: {
      auth: true,
      breadcrumb: 'Show'
    },
    children: [
      { path: '', name: 'buy.show.overview', component: () => import('@/pages/Buy/Show/_Overview/Index'), meta: { auth: true } },
      { path: 'statistics', name: 'buy.show.statistics', component: () => import('@/pages/Buy/Show/_Statistics/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
      { path: 'proposition', name: 'buy.show.propositions', component: () => import('@/pages/Buy/Show/_Propositions/Index'), meta: { auth: true, breadcrumb: 'Propositions' } },
      { path: 'settings', name: 'buy.show.settings', component: () => import('@/pages/Buy/Show/_Settings/Index'), meta: { auth: true, breadcrumb: 'Settings' } }
    ]
  },

  // Buy
  {
    path: '/rent/',
    component: () => import('@/pages/Rent/Index/Index'),
    name: 'rent.index',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Renting proposal'
    }
  },
  // Rent list
  {
    path: '/rent/list',
    name: 'rent.list',
    component: () => import('@/pages/Rent/List/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Renting proposal List'
    }
  },

  // Profile new rent
  {
    path: '/rent/new',
    name: 'rent.new',
    component: () => import('@/pages/Rent/New/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Renting proposal New'
    }
  },
  {
    path: '/rent/overview',
    name: 'rent.overview',
    component: () => import('@/pages/Rent/Overview/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Overview'
    }
  },
  {
    path: '/rent/:id(\\d+)/edit',
    name: 'rent.edit',
    component: () => import('@/pages/Rent/Edit/Index'),
    meta: {
      auth: true,
      breadcrumb: 'Show'
    }
  },
  {
    path: '/rent/:id(\\d+)',
    component: () => import('@/pages/Rent/Show/Index'),
    meta: {
      auth: false,
      breadcrumb: 'Show'
    },
    children: [
      { path: '', name: 'rent.show.overview', component: () => import('@/pages/Rent/Show/_Overview/Index'), meta: { auth: false, breadcrumb: 'Statistics' } },
      { path: 'statistics', name: 'rent.show.statistics', component: () => import('@/pages/Rent/Show/_Statistics/Index'), meta: { auth: true, breadcrumb: 'Statistics' } },
      { path: 'proposition', name: 'rent.show.propositions', component: () => import('@/pages/Rent/Show/_Propositions/Index'), meta: { auth: true, breadcrumb: 'Propositions' } },
      { path: 'settings', name: 'rent.show.settings', component: () => import('@/pages/Rent/Show/_Settings/Index'), meta: { auth: true, breadcrumb: 'Settings' } }
    ]
  },

  // Sell
  {
    path: '/sell/',
    component: () => import('@/pages/Sell/Index/Index'),
    name: 'sell.index',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Selling'
    }
  },

  // Sell
  {
    path: '/sell/new',
    component: () => import('@/pages/Sell/New/Index'),
    name: 'sell.new',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Selling new'
    }
  },

  // Lease
  {
    path: '/lease/',
    component: () => import('@/pages/Lease/Index/Index'),
    name: 'lease.index',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Leasing'
    }
  },

  // Buy
  {
    path: '/lease/new',
    component: () => import('@/pages/Lease/New/Index'),
    name: 'lease.new',
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Leasing'
    }
  },

  // Profile get_started
  {
    path: '/profile/get_started',
    name: 'profile.getstarted',
    component: () => import('@/pages/Profile/Get/Started/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Get started'
    }
  },

  // Feeds
  {
    path: '/feeds/',
    name: 'feeds',
    component: () => import('@/pages/Feeds/Index'),
    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
      breadcrumb: 'Feed'
    }
  },

  // Account
  {
    path: '/account',
    name: 'account.index',
    component: () => import('@/pages/Account/Index'),

    // If the user needs to be authenticated to view this page.
    meta: {
      auth: true
    }
  },

  // Recover password
  {
    path: '/recover/password',
    name: 'recover.password.index',
    component: () => import('@/pages/Recover/Password/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Recover password'
    }
  },

  // Login
  {
    path: '/login',
    name: 'login.index',
    component: () => import('@/pages/Login/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: false
    }
  },

  // Register
  {
    path: '/register',
    name: 'register.index',
    component: () => import('@/pages/Register/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      auth: false
    }
  },

  // Activate account
  {
    path: '/activate',
    name: 'activate.index',
    component: () => import('@/pages/Activate/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: false,
      breadcrumb: 'Activate'
    }
  },

  // Profile get_information
  /* {
    path: '/profile/get_information',
    name: 'profile.getinformation',
    component: () => import('@/pages/Profile/Get/Information/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'Get information'
    }
  }, */

  // profile new rent
  {
    path: '/profile/',
    component: () => import('@/pages/Profile/Index'),
    // If the user needs to be a guest to view this page.
    meta: {
      auth: true,
      breadcrumb: 'profile'
    },
    children: [
      // Profile show
      {
        path: '/:id',
        name: 'profile.show',
        component: () => import('@/pages/Profile/Get/Information/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'Get information'
        }
      },
      // Profile get_information
      {
        path: '/profile/get_information',
        name: 'profile.getinformation',
        component: () => import('@/pages/Profile/Get/Information/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'Get information'
        }
      },
      {
        path: 'edit',
        meta: {
          breadcrumb: 'edit'
        },
        component: () => import('@/pages/Profile/_Edit/Index'),
        children: [
          { path: '', name: 'profile.edit', component: () => import('@/pages/Profile/_Edit/_Index/Index'), meta: { auth: true } },
          { path: 'password', name: 'profile.edit.password', component: () => import('@/pages/Profile/_Edit/_Password/Index'), meta: { auth: true, breadcrumb: 'Password' } },
          { path: 'contact', name: 'profile.edit.contact', component: () => import('@/pages/Profile/_Edit/_Contact/Index'), meta: { auth: true, breadcrumb: 'Contact' } },
          { path: 'role', name: 'profile.edit.role', component: () => import('@/pages/Profile/_Edit/_Role/Index'), meta: { auth: true, breadcrumb: 'Role' } },
          { path: 'information', name: 'profile.edit.information', component: () => import('@/pages/Profile/_Edit/_Information/Index'), meta: { auth: true, breadcrumb: 'Information' } }
        ]
      },
      // profile show own
      {
        path: '',
        name: 'profile.own',
        component: () => import('@/pages/Profile/_Index/Index'),
        // If the user needs to be a guest to view this page.
        meta: {
          auth: true,
          breadcrumb: 'buying proposal overview'
        }
      }
    ]
  }

  /* {
    path: '/*',
    redirect: '/home'
  } */
]

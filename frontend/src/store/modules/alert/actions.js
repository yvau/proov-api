/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import * as types from './mutation-types'

export const message = ({ commit }, payload) => {
  commit(types.MESSAGE, payload)
}

export default {
  message
}

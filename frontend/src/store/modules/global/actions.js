/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import store from '@/store/index'
import Proxy from '@/proxies/Proxy'
import service from '@/services/index'
import * as types from './mutation-types'

export const submit = ({ commit }, payload) => {
  let button = payload.button
  if (button !== undefined) {
    button.classList.add('loading')
  }
  store.dispatch('alert/message', {message: null, status: false, success: null})
  new Proxy()
    .submit(payload.method, payload.url, payload.data)
    .then((response) => {
      if (button !== undefined) {
        button.classList.remove('loading')
      }
      if (response.success) {
        store.dispatch('alert/message', {message: response.message, status: true, success: response.success})
      } else {
        service.errorMessage(response)
      }
    })
    .catch(() => {
      if (button !== undefined) {
        button.classList.remove('loading')
      }
      console.log('Request failed...')
    })
}

export const setObject = ({ commit }, payload) => {
  commit(types.OBJECT, payload)
}

export const setList = ({ commit }, payload) => {
  commit(types.LIST, payload)
}

export const find = ({ commit }, payload) => {
  new Proxy(payload.url, {})
    .find(payload.data)
    .then((response) => {
      try {
        let content = (payload.transformer === undefined) ? response.content : payload.transformer.fetch(response)
        commit(types.OBJECT, content)
      } catch (err) {
        // console.log(err)
      }
    })
    .catch(() => {
      console.log('Request failed...')
    })
}

export const findAll = ({ commit }, payload) => {
  commit(types.LOADING, true)
  new Proxy(payload.url, {})
    .setParameters(payload.parameters)
    .all()
    .then((response) => {
      try {
        commit(types.LIST, response)
        commit(types.LOADING, false)
      } catch (err) {
        console.log(err)
      }
    })
    .catch(() => {
      commit(types.LOADING, false)
      console.log('Request failed...')
    })
}

export const loading = ({ commit }, payload) => {
  commit(types.LOADING, payload)
}

export default {
  submit,
  findAll,
  loading,
  setObject,
  setList,
  find
}

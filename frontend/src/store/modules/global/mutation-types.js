/* ============
 * Mutation types for the account module
 * ============
 *
 * The mutation types that are available
 * on the auth module.
 */

export const LIST = 'LIST'
export const OBJECT = 'OBJECT'
export const LOADING = 'LOADING'

export default {
  LIST,
  OBJECT,
  LOADING
}

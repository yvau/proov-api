/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import {
  LIST,
  OBJECT,
  LOADING
} from './mutation-types'

export default {
  [LIST] (state, payload) {
    state.list.push(payload)
  },
  [OBJECT] (state, payload) {
    state.object = payload
  },
  [LOADING] (state, payload) {
    state.loading = payload
  }
}

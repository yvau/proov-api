import { createApp } from './main'

// client-specific bootstrapping logic...
import './plugins/stylesheet'
import './plugins/font-awesome'
import './plugins/multiselect'
import './plugins/vue-component'
import './plugins/simple-vue-validator'
import './plugins/vue-tour'
import './plugins/pace'

/*
import './plugins/jquery'
 */

const { app, store } = createApp()

if (window.__INITIAL_STATE__) {
  store.replaceState(window.__INITIAL_STATE__)
}
// this assumes App.vue template root element has `id="app"`
app.$mount('#app')

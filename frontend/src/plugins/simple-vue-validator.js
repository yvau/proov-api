/* ============
 * Vue Simple Validation
 * ============
 *
 * SimpleVueValidation
 *
 * https://github.com/semisleep/simple-vue-validator
 */

import Vue from 'vue'
import SimpleVueValidation from 'simple-vue-validator'
Vue.use(SimpleVueValidation)

/* ============
 * fortawesome
 * ============
 *
 * Fort Awesome
 *
 * https://fontawesome.com/icons/
 */
import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleRight, faSignOutAlt, faUserShield, faHome, faChevronDown, faBuilding, faAddressCard, faStar, faBath, faBed, faArrowsAlt, faEdit, faTrashAlt, faEye } from '@fortawesome/free-solid-svg-icons'
import { faBell, faCheckCircle, faBuilding as faBuildingReg, faAddressCard as faAddressCardReg } from '@fortawesome/free-regular-svg-icons'
import { faFacebookF, faLinkedinIn, faTwitter, faGoogle } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faAngleRight, faEye, faChevronDown, faEdit, faTrashAlt, faBell, faUserShield, faHome, faBuilding, faBuildingReg, faStar, faBath, faBed, faArrowsAlt, faAddressCardReg, faAddressCard, faSignOutAlt, faFacebookF, faCheckCircle, faLinkedinIn, faTwitter, faGoogle)

Vue.component('font-awesome-icon', FontAwesomeIcon)

/* ============
 * Vue Tour
 * ============
 *
 * VueTour
 *
 * https://github.com/pulsardev/vue-tour
 */
import Vue from 'vue'
import VueTour from 'vue-tour'
import 'vue-tour/dist/vue-tour.css'

// register globally
Vue.use(VueTour)

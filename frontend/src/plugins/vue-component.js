import Vue from 'vue'
import VueNumeric from 'vue-numeric'

Vue.component('message-alert', () => import('@/components/message/Alert'))
// used to show modal forgot password
Vue.component('modal-forgot-password', () => import('@/components/modal/ModalForgotPassword'))
// used to show modal price package
Vue.component('modal-price-package', () => import('@/components/modal/ModalPricePackage'))
// used to show modal proposal interest
Vue.component('modal-proposal-interest', () => import('@/components/modal/ModalProposalInterest'))
// used to show chart
Vue.component('widget-chart', () => import('@/components/chart/WidgetStatisticMedium'))
// used to make the dropdown of user menu
Vue.component('main-dropdown', () => import('@/components/dropdown/mainDropdown'))
// used to display menu layout
Vue.component('menu-layout', () => import('@/components/Menu'))
// Vue masked input
Vue.component('masked-input', () => import('vue-masked-input'))
// Vue peity
Vue.component('peity', () => import('vue-peity'))

// Vue numeric input
Vue.use(VueNumeric)

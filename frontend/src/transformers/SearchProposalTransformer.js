/* ============
 * SearchProposal Transformer
 * ============
 *
 * The transformer for the account.
 */

import Transformer from './Transformer'

export default class SearchProposalTransformer extends Transformer {
  /**
   * Method used to transform a fetched account.
   *
   * @param account The fetched account.
   *
   * @returns {Object} The transformed account.
   */
  /* static fetch (account) {
    return {
      email: account.credential,
      firstName: account.profile_information_id.first_name,
      lastName: account.profile_information_id.last_name,
      photoUrl: (account.profile_information_id.profile_photo_id === null) ? '/static/images/svg/user.svg' : account.profile_information_id.profile_photo_id.url
    }
  } */

  /**
   * Method used to transform a send account.
   *
   * @param account The account to be send.
   *
   * @returns {Object} The transformed account.
   */
  static send (searchProposal) {
    return {
      location: (searchProposal.location === null) ? null : searchProposal.location.map(function (x) { return x.id }).toString(),
      typeOfProperty: searchProposal.typeOfProperty.toString(),
      categoryOfProperty: searchProposal.categoryOfProperty,
      isUrgent: searchProposal.isUrgent,
      ageOfProperty: searchProposal.ageOfProperty,
      amenity: searchProposal.amenity.toString(),
      isFurnished: searchProposal.isFurnished,
      priceMinimum: searchProposal.priceMinimum,
      priceMaximum: searchProposal.priceMaximum,
      isPreApproved: searchProposal.isPreApproved,
      isPreQualified: searchProposal.isPreQualified,
      isFirstBuyer: searchProposal.isFirstBuyer
      // urgent: Util.setUrgent(searchProposal.urgent),
      // isFurnished: Util.setFurnished(searchProposal.isFurnished),
      // bathrooms: searchProposal.bathrooms,
      // bedrooms: searchProposal.bedrooms,

      // typeOfProposal: searchProposal.typeOfProposal,
      // status: searchProposal.status,
      // ageOfProperty: searchProposal.ageOfProperty,
      // priceMinimum: searchProposal.priceMinimum,
      // priceMaximum: searchProposal.priceMaximum,
      // size: searchProposal.size,

    }
  }
}

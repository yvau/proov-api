/* ============
 * Property Transformer
 * ============
 *
 * The transformer for the property.
 */

import Transformer from './Transformer'
import store from '@/store/index'

export default class PropertyTransformer extends Transformer {
  /**
   * Method used to transform a fetched property.
   *
   * @param property The fetched property.
   *
   * @returns {Object} The transformed property.
   */
  static fetch (property) {
    return {
      id: property.content.id,
      location: property.content.location,
      country: property.content.location.country.name,
      province: property.content.location.province.name,
      city: property.content.location.city.name,
      address: property.content.location.address,
      postalCode: property.content.location.postal_code,
      saleType: property.content.saleType,
      bathrooms: property.content.bathrooms,
      bedrooms: property.content.bedrooms,
      status: property.content.status,
      characteristics: property.content.characteristics.split(','),
      dateOfCreation: property.content.dateOfCreation,
      description: property.content.description,
      type: property.content.ype,
      price: property.content.price,
      size: property.content.size
    }
  }

  /**
   * Method used to transform a send role.
   *
   * @param role The role to be send.
   *
   * @returns {Object} The transformed role.
   */
  static send (property) {
    let formData = new FormData()
    let images = store.state.global.list
    for (let x in images) {
      formData.append('propertyPhotos[]', images[x].file)
    }
    formData.append('status', property.status)
    formData.append('characteristics', property.characteristics.toString())
    formData.append('description', property.description)
    formData.append('address', property.address)
    formData.append('postalCode', property.postalCode)
    formData.append('bathrooms', property.bathrooms)
    formData.append('bedrooms', property.bedrooms)
    formData.append('price', property.price)
    formData.append('saleType', property.saleType)
    formData.append('size', property.size)
    formData.append('type', property.type)
    formData.append('location', (property.location.id === undefined) ? '' : property.location.id.toString())
    return formData
  }
}

'use strict'
require('./check-versions')()

process.env.NODE_ENV = 'production'

const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const shell = require('shelljs')
const webpackConfig = require('./webpack.server.conf')

const spinner = ora('building server for production...')
spinner.start()

  webpack(webpackConfig, function (err, stats) {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')

  // shell.sed('-i', '/public/stylesheets', '/stylesheets', 'dist/index.html')
  shell.rm('-rf', '../server_render/')
  shell.rm('-rf', '../public/static/stylesheets/')
  shell.rm('-rf', '../public/static/javascripts/')
  shell.rm('-rf', '../public/static/images/')

  shell.mkdir('-p', '../server_render/')
  shell.sed('-i', '</script>', '</script>\n', 'dist/index.ejs')
  shell.sed('-i', '</script><script', '</script>\n<script', 'dist/index.ejs')
  shell.sed('-i', '</script></body>', '</script>\n</body>', 'dist/index.ejs')
  shell.sed('-i', '<div id="app"></div>', '<!--vue-ssr-outlet-->', 'dist/index.ejs')
  shell.sed('-i', '/public', '', 'dist/index.ejs')
  shell.mv(['dist/static/stylesheets/', 'dist/static/javascripts/', 'dist/static/images/'], '../public/static/')
  shell.mv(['dist/server.bundle.js'], '../server_render/')
  shell.mv(['dist/index.ejs'], '../views/')


    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      process.exit(1)
    }

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow('  Server chunk file ready.\n'))
  })

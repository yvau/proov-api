let express = require('express')
let router = express.Router()
let bcrypt = require('bcrypt-nodejs')
let utils  = require('../services/utils')
let model = require('../models/index')
const email = require('../services/email')
const auth = require('../services/authentication')
let validator  = require('../validator/AuthValidator')
let { validationResult } = require('express-validator/check')
let passportFacebook = require('../services/facebook')
let passportTwitter = require('../services/twitter')
let passportGoogle = require('../services/google')
let passportLinkedin = require('../services/linkedin')

/**
* @api {POST} /auth/register Registration api
* @apiName Register
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} send email to user.
* @apiSuccess {String} message Message from API.
*/
router.post('/register', new validator().setRegister(), function(req, res, next) {
  // get the ip address
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress

  // const for validation result
  const errors = validationResult(req)

  // show errors if the form doesn't meet the validation
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
  }
  // save data to profile information
  model.profile_information.create({
    first_name: req.body.firstName,
    last_name: req.body.lastName,
    birth_date: req.body.birthDate,
    email_contact: req.body.credential
  }).then(profileInformation => {
    // save data to profile information
    model.profile.create({
      account_non_expired: true,
      account_non_locked: true,
      credential: req.body.credential,
      credentials_non_expired: true,
      date_of_creation: Date.now(),
      date_of_creation_token: Date.now(),
      enabled: false,
      ip_address: ip,
      password: bcrypt.hashSync(req.body.password),
      role: 'ROLE_LIMITED',
      token: utils.generateToken(),
      profile_information_id: profileInformation.dataValues.id
    }).then(profile => {
      email.sendMail({login: profile.credential, lastName: profileInformation.last_name, token: profile.token}, 'user registration', 'views/registration.ejs')
      return successMessage(res, {message : `an email has been sent to ${profile.credential}. click on the link to activate your account`})
    })
  })
})

/**
* @api {GET} /auth/reset?t=######## Reset api
* @apiName Reset
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} send email to user.
* @apiSuccess {String} message Message from API.
*/
router.get('/reset', async function(req, res, next) {
  if(!req.query.t) {
    return errorMessage(res, {message : 'something wrong with the token'})
  }

  [err, profile] = await to(model.profile.findOne({ where: { token: req.query.t } }))

  if(err){
    return errorMessage(res, {message : err})
  }

  if (!profile) {
    return errorMessage(res, {message : 'the token has expired'})
  }

  return successMessage(res, {message : ''})
})

/**
* @api {POST} /auth/reset Registration api
* @apiName Register
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} send email to user.
* @apiSuccess {String} message Message from API.
*/
router.post('/reset', new validator().setReset(), async function(req, res, next) {

  // const for validation result
  const errors = validationResult(req)

  // show errors if the form doesn't meet the validation
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
  }

  if(!req.body.token) {
    return errorMessage(res, {message : 'something wrong with the token'})
  }

  [err, profile] = await to(model.profile.findOne({ where: { token: req.body.token } }))

  // update profile table give a token to update password
  model.profile.update({
    token: null,
    password: bcrypt.hashSync(req.body.password)
  }, {where: {credential: req.body.token}}
  ).then(profile => {

  })

  const userObject = { id: profile.id, login: profile.credential, time: new Date() }
  token = auth.issueToken(userObject)

  return successMessage(res, {message : token})
})


/**
* @api {GET} /auth/login Login
* @apiName login
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} message Message from API.
*/
router.post('/login', async function(req, res, next) {
  [err, token] = await to(auth.login(req.body))
  if(err) {
    return errorMessage(res, err)
  }
  return successMessage(res, {message : token})
})


/**
* @api {GET} /auth/recover Login
* @apiName recover
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} message Message from API.
*/
router.post('/recover', new validator().setRecover(), function(req, res, next) {

  // const for validation result
  const errors = validationResult(req)

  // show errors if the form doesn't meet the validation
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
  }

  // update profile table give a token to update password
  model.profile.update(
    {token: utils.generateToken()},
    {where: {credential: req.body.credential}}
  ).then(profile => {

  })

  // query to load data from entity
  model.profile.findOne({
    where: {credential: req.body.credential}
  }).then(function(profile) {

    // send email to user
    email.sendMail(profile, 'user recover', 'views/forgot_password.ejs')

    // render data from query
    return successMessage(res, {message : 'a mail has been sent to you'})
  })
})

/**
* @api {GET} /auth/activate?t=######### Activate account
* @apiName activate
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} message Message from API.
*/
router.get('/activate', async function(req, res, next) {
  if(!req.query.t) {
    return errorMessage(res, {message : 'something wrong with the token'})
  }

  [err, profile] = await to(model.profile.findOne({ where: { token: req.query.t } }))

  if(err){
    return errorMessage(res, {message : err})
  }

  if (!profile) {
    return errorMessage(res, {message : 'the token has expired'})
  }

  // update profile table give a token to update password
  [err, profileUpdate] = await to(model.profile.update({
    token: null,
    enabled: true },
    {where: {id: profile.id}}
  ))

  const userObject = { id: profile.id, login: profile.credential, time: new Date() }
  token = auth.issueToken(userObject)

  return successMessage(res, {message : token})
})

/**
* @api {POST} /auth/facebook/login Social Login
* @apiName google
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} message Message from API.
*/
router.get('/facebook/login',
  passportFacebook.authenticate('facebook'),
  function(req, res, next) {
    return res.json('ezvrf')
})
router.get('/facebook/callback',
  passportFacebook.authenticate('facebook', { failureRedirect: '/login' }),
  function(req, res, next) {
  return res.redirect('/')
})

/**
* @api {POST} /auth/login/google Social Login
* @apiName google
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} message Message from API.
*/
router.get('/google/login',
  passportGoogle.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }))

router.get('/google/callback',
  passportGoogle.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
  res.redirect('/')
})

/**
* @api {POST} /auth/twitter/login Social Login
* @apiName twitter
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} message Message from API.
*/
router.get('/twitter/login',
  passportTwitter.authenticate('twitter', { scope: ['include_email=true']}),
  function(req, res, next) {
  return res.json('ezvrf')
})
router.get('/twitter/callback',
  passportTwitter.authenticate('twitter', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/')
})


/**
* @api {POST} /auth/linkedin/login Social Login
* @apiName linkedin
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} message Message from API.
*/
router.get('/linkedin/login',
  passportLinkedin.authenticate('linkedin'),
  function(req, res, next) {
    return res.json('ezvrf')
})
router.get('/linkedin/callback',
  passportLinkedin.authenticate('linkedin', { failureRedirect: '/login' }),
  function(req, res, next) {
    res.redirect('/')
    // next()
})

module.exports = router

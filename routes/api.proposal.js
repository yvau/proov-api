var express = require('express')
var router = express.Router()
var maxmind = require('maxmind')
var model = require('../models/index')
let utils  = require('../services/utils')
const Query = require('../query/QueryProposal')
let permit = require('../services/auth.policy').permit
let validator  = require('../validator/ProposalValidator')
const { validationResult } = require('express-validator/check')


/**
* @api {GET} /api/proposals/list Proposal List api
* @apiName ProposalList
* @apiGroup Api
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} list proposals.
* @apiSuccess {Integer} count proposals.
*/
router.get('/proposals/list', async function(req, res, next) {
  model.proposal.belongsToMany(model.location, {through: model.proposal_has_location, foreignKey: 'proposal_id', constraints: false})
  model.location.belongsToMany(model.proposal, {through: model.proposal_has_location,  foreignKey: 'location_id', constraints: false})
  model.location.belongsTo(model.city, {foreignKey: 'city_id'})
  // init limitation row according to page
  let limit = (req.query.size === undefined) ? 8 : req.query.size
  let offset = (req.query.page === undefined) ? 0 : limit * (req.query.page - 1)

  // lookup for city object according to ip address
  let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress

  // query to load data from entity
  model.proposal.findAndCountAll({
    where: Query.fetch(req.query),
    include: [ { model: model.location, required: true,
      include: [
        {model: model.city}
      ]
    } ],
    order: [
      ['id', 'DESC']
    ],
    limit: limit,
    offset: offset
  }).then(function(entity) {

    // render data from query
    maxmind.open('./mmdb/GeoLite2-City.mmdb', (err, cityLookup) => {
      var organization = cityLookup.get(ip)
      let valueCity
      if (organization === undefined || organization === null){
        valueCity = '---'
      } else {
        valueCity = organization.city.names['en']
      }
      res.json({'content': entity.rows, 'count': entity.count, 'variable': valueCity})
    });

  })
})



/**
* @api {GET} /api/p/proposals/list Proposal List api
* @apiName PProposalList
* @apiGroup Api
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} list proposals.
* @apiSuccess {Integer} count proposals.
*/
router.get('/p/proposals/list', permit(null, 'ROLE_BUYER','ROLE_TENANT'), async function(req, res, next) {
  model.proposal.belongsToMany(model.location, {through: model.proposal_has_location, foreignKey: 'proposal_id', constraints: false})
  model.location.belongsToMany(model.proposal, {through: model.proposal_has_location,  foreignKey: 'location_id', constraints: false})
  model.location.belongsTo(model.city, {foreignKey: 'city_id'})
  // init limitation row according to page
  let limit = (req.query.size === undefined) ? 8 : req.query.size
  let offset = (req.query.page === undefined) ? 0 : limit * (req.query.page - 1)

  // assign default values to filter
  req.query.user_id = res.user.id // to fetch only data from current user
  req.query.status = (req.query.status === undefined) ? 'all' : req.query.status

  // query to load data from entity
  model.proposal.findAndCountAll({
    where: Query.fetch(req.query),
    include: [ { model: model.location, required: true,
      include: [
        {model: model.city}
      ]
    } ],
    order: [
      ['id', 'DESC']
    ],
    limit: limit,
    offset: offset
  }).then(function(entity) {

    // render data from query
    res.json({'content': entity.rows, 'count': entity.count})
  })
})


/**
* @api {GET} /api/buy/new Buy new api
* @apiName BuyNew
* @apiGroup Api
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} success message.
*/
router.post('/buy/new',
  permit(null, 'ROLE_BUYER'),
  new validator().buyNew(),
  async function(req, res, next) {
    //
    const errors = validationResult(req)
    // check if there is errors
    if (!errors.isEmpty()) {
      let paramObject = {'customArray': errors.array(), 'prop': 'param'}
      return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
    }

    // create buying proposal entity
    let buy = await model.proposal.create({
      age_of_property: req.body.ageOfProperty,
      bathrooms: req.body.bathrooms,
      bedrooms: req.body.bedrooms,
      enabled: true,
      features: req.body.features,
      is_urgent: req.body.urgent,
      price_maximum: req.body.priceMaximum,
      price_minimum: req.body.priceMinimum,
      size: req.body.size,
      status: req.body.status,
      date_of_creation: Date.now(),
      type_of_property: req.body.typeOfProperty,
      type_of_proposal: req.body.typeOfProposal,
      profile_id: res.user.id
    })

    // let city, err
    model.city.belongsTo(model.province, {foreignKey: 'province_id'})
    model.province.belongsTo(model.country, {foreignKey: 'country_id'})

    // get how many location the renting proposal is about
    for (let i = 0; i < req.body.location.split(',').length; i++) {

      // query to load location from entity
      let find_location = await model.city.findOne({ where: {id: req.body.location.split(',')[i]},
        include: [{ model: model.province, include: [{ model: model.country }] }] })

      // create location entity
      let location = await model.location.create({
        id: utils.generateToken(),
        country_id: find_location.province.country.id,
        province_id: find_location.province_id,
        city_id: find_location.id
      })

      // create proposal_has_location entity
      let proposal_has_location = await model.proposal_has_location.create({
        proposal_id: buy.id,
        location_id: location.id
      })
    }
    // if(err) throwError('error while updating the password')

    return successMessage(res, {message : 'Your buying proposal has been added successfully'})
})


/**
* @api {GET} /api/rent/new Rent new api
* @apiName RentNew
* @apiGroup Api
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} success message.
*/
router.post('/rent/new',
  permit(null, 'ROLE_TENANT'),
  new validator().rentNew(),
  async function(req, res, next) {
    //
    const errors = validationResult(req)
    // check if there is errors
    if (!errors.isEmpty()) {
      let paramObject = {'customArray': errors.array(), 'prop': 'param'}
      return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
    }

    // create renting proposal entity
    let rent = await model.proposal.create({
      age_of_property: req.body.ageOfProperty,
      bathrooms: req.body.bathrooms,
      bedrooms: req.body.bedrooms,
      enabled: true,
      features: req.body.features,
      is_urgent: req.body.urgent,
      price_maximum: req.body.priceMaximum,
      price_minimum: req.body.priceMinimum,
      size: req.body.size,
      status: req.body.status,
      date_of_creation: Date.now(),
      type_of_property: req.body.typeOfProperty,
      type_of_proposal: req.body.typeOfProposal,
      profile_id: res.user.id
    })

    // let city, err
    model.city.belongsTo(model.province, {foreignKey: 'province_id'})
    model.province.belongsTo(model.country, {foreignKey: 'country_id'})

    // get how many location the renting proposal is about
    for (let i = 0; i < req.body.location.split(',').length; i++) {

      // query to load location from entity
      let find_location = await model.city.findOne({ where: {id: req.body.location.split(',')[i]},
        include: [{ model: model.province, include: [{ model: model.country }] }] })

      // create location entity
      let location = await model.location.create({
        id: utils.generateToken(),
        country_id: find_location.province.country.id,
        province_id: find_location.province_id,
        city_id: find_location.id
      })

      // create proposal_has_location entity
      let proposal_has_location = await model.proposal_has_location.create({
        proposal_id: rent.id,
        location_id: location.id
      })
    }
    // if(err) throwError('error while updating the password')

    return successMessage(res, {message : 'Your renting proposal has been added successfully'})
})

module.exports = router

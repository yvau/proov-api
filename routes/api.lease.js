let express = require('express')
let router = express.Router()
let model = require('../models/index')
let utils  = require('../services/utils')
let permit = require('../services/auth.policy').permit
let validator  = require('../validator/SearchProposalValidator')
const { validationResult } = require('express-validator/check')


/**
* @api {POST} /auth/reset Registration api
* @apiName Register
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} send email to user.
* @apiSuccess {String} message Message from API.
*/
router.post('/lease/new',
  permit(null, 'ROLE_LESSOR'),
  new validator().leaseForm(),
  async function(req, res, next) {
    //
    const errors = validationResult(req)
    // check if there is errors
    if (!errors.isEmpty()) {
      let paramObject = {'customArray': errors.array(), 'prop': 'param'}
      return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
    }

  // create search proposal entity
  let search_proposal = await model.search_proposal.create({
    age_of_property: req.body.ageOfProperty,
    amenity: req.body.amenity,
    category_of_property: req.body.categoryOfProperty,
    has_garage: req.body.hasGarage,
    has_lift: req.body.hasLift,
    has_parking: req.body.hasParking,
    has_pool: req.body.hasPool,
    is_first_buyer: req.body.isFirstBuyer,
    is_furnished: req.body.isFurnished,
    is_near_navigable_body_of_water: req.body.isNearNavigableBodyOfWater,
    is_near_parc: req.body.isNearParc,
    is_near_public_transport: req.body.isNearPublicTransport,
    is_near_trade_square : req.body.isNearTradeSquare,
    is_suitable_for_reduced_mobility: req.body.isSuitableForReducedMobility,
    is_urgent: req.body.isUrgent,
    is_without_contingency: req.body.isWithoutContingency,
    price_maximum: req.body.priceMaximum,
    price_minimum: req.body.priceMinimum,
    type_of_property: req.body.typeOfProperty,
    type_of_proposal: 'for_rent',
    type_of_search: req.body.typeOfSearch,
    profile_id: res.user.id
  })

  // let city, err
  model.city.belongsTo(model.province, {foreignKey: 'province_id'})
  model.province.belongsTo(model.country, {foreignKey: 'country_id'})

  // get how many location the renting proposal is about
  for (let i = 0; i < req.body.location.split(',').length; i++) {

    // query to load location from entity
    let find_location = await model.city.findOne({ where: {id: req.body.location.split(',')[i]},
      include: [{ model: model.province, include: [{ model: model.country }] }] })

    // create location entity
    let location = await model.location.create({
      id: utils.generateToken(),
      country_id: find_location.province.country.id,
      province_id: find_location.province_id,
      city_id: find_location.id
    })

    // create proposal_has_location entity
    let search_has_location = await model.search_has_location.create({
      search_proposal_id: search_proposal.id,
      location_id: location.id
    })
  }
  // if(err) throwError('error while creating the property')

  // render data from query
  return successMessage(res, {message : 'the selling profile has been created'})
})

/**
* @api {PUT} /auth/reset Registration api
* @apiName Register
* @apiGroup Auth
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} send email to user.
* @apiSuccess {String} message Message from API.
*/
router.put('/lease/update',
  permit('search_proposal'),
  new validator().leaseForm(),
  async function(req, res, next) {
    //
    const errors = validationResult(req)
    // check if there is errors
    if (!errors.isEmpty()) {
      let paramObject = {'customArray': errors.array(), 'prop': 'param'}
      return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
    }

    // update data to profile model
  [err, search_proposal] = await to(model.search_proposal.create({
    age_of_property: req.body.ageOfProperty,
    amenity: req.body.amenity,
    category_of_property: req.body.categoryOfProperty,
    has_garage: req.body.hasGarage,
    has_lift: req.body.hasLift,
    has_parking: req.body.hasParking,
    has_pool: req.body.hasPool,
    is_first_buyer: req.body.isFirstBuyer,
    is_furnished: req.body.isFurnished,
    is_near_navigable_body_of_water: req.body.isNearNavigableBodyOfWater,
    is_near_parc: req.body.isNearParc,
    is_near_public_transport: req.body.isNearPublicTransport,
    is_near_trade_square : req.body.isNearTradeSquare,
    is_pre_approved: req.body.isPreApproved,
    is_pre_qualified: req.body.isPreQualified,
    is_suitable_for_reduced_mobility: req.body.isSuitableForReducedMobility,
    is_urgent: req.body.isUrgent,
    is_without_contingency: req.body.isWithoutContingency,
    price_maximum: req.body.priceMaximum,
    price_minimum: req.body.priceMinimum,
    type_of_property: req.body.typeOfProperty,
    type_of_proposal: req.body.typeOfProposal,
    type_of_search: req.body.typeOfSearch,
    profile_id: req.user.id,

  }, {where: {id: req.body.id}}))
  if(err) throwError('error while creating the property')

  // render data from query
  return successMessage(res, {message : 'the selling profile has been added'})
})

module.exports = router

var express = require('express')
var router = express.Router()
var model = require('../models/index')
var paypalRest = require('../services/paypal').paypal
var stripe = require("stripe")("sk_test_ZOBV6GGya2WPzKxmWMcm1ACF")
const Query = require('../query/QueryPackage')



/* GET city page. */
router.get('/package/list', async function(req, res, next) {
  let err, package_product

  // update data to profile model
  [err, package_product] = await to(model.package_product.findAndCountAll({
    where: Query.fetch(req.query),
    order: [
      ['id', 'ASC']
    ]
  }))
  if(err) {
    return errorMessage(res, err)
  }
  // render final message
  return successMessage(res, {'content': package_product.rows, 'count': package_product.count})
})

/* GET city page. */
router.get('/payment/paypal', async function(req, res, next) {
  let err, package_product

  // update data to profile model
  [err, package_product] = await to(model.package_product.findAndCountAll({
    where: Query.fetch(req.query),
    order: [
      ['id', 'ASC']
    ]
  }))
  if(err) {
    return errorMessage(res, err)
  }
  // render final message
  return successMessage(res, {'content': package_product.rows, 'count': package_product.count})
})

// start payment process 
router.post('/payment/paypal' , function ( req , res, next ) {
  // create payment object
  let payment = {
    "intent": "authorize",
    "payer": {
      "payment_method": "paypal"
	  },
    "redirect_urls": {
      "return_url": "http://127.0.0.1:3000/success",
      "cancel_url": "http://127.0.0.1:3000/err"
	  },
	  "transactions": [{
		  "amount": {
			  "total": 39.00,
			  "currency": "USD"
		  },
		  "description": " a book on mean stack "
	  }]
  }
	
	
	paypalRest.payment.create(payment , function( err , payment ) {
    if ( err ) {
      return errorMessage(res, {message : err})
    } else {
      return successMessage(res, {message : payment.id})
    }
  })
})

// start payment process 
router.post('/payment/charge' , function ( req , res, next ) {
  // create payment object
  let paymentId = req.body.paymentID
  let payerId = req.body.payerID

  let execute_payment_json = {
    "payer_id": payerId,
    "transactions": [{
      "amount": {
        "currency": "USD",
        "total": 39.00
      }
    }]
  }

  paypalRest.payment.execute(paymentId, execute_payment_json, function (error, payment) {
    if (error) {
      return errorMessage(res, {message : err})
    } else {
      console.log(JSON.stringify(payment))
    }
  })
})


// start payment process 
router.post('/payment/pay' , function ( req , res, next ) {
  // create payment object
  stripe.charges.create({
    amount: req.body.amount,
    currency: "usd",
    source: req.body.token, // obtained with Stripe.js
    description: req.body.description
  }, function(err, charge) {
    // asynchronously called
    return successMessage(res, {message : charge.source})
  })
})



module.exports = router

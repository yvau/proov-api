var express = require('express')
// var template = require('../services/render')
var router = express.Router()

/**
* @api {GET} / Feeds page
*/
router.get('/', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  // template.render(context, req, res, next)
})

module.exports = router

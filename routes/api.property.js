var express = require('express')
var router = express.Router()
var model = require('../models/index')
let validator  = require('../validator/PropertyValidator')
const { validationResult } = require('express-validator/check')
const utils = require('../services/utils')
const Query = require('../query/QueryProperty')
const permit = require('../services/auth.policy').permit
const cloudinary = require('../services/cloudinary')
var multer  = require('multer')
var upload = multer({ dest: './uploads/' })




router.post('/property/new',
  upload.array('propertyPhotos[]'),
  permit(null, 'ROLE_SELLER','ROLE_LESSOR'),
  new validator().property(),
  async function(req, res, next) {

  //
  const errors = validationResult(req)
  // check if there is errors
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
  }
  model.city.belongsTo(model.province, {foreignKey: 'province_id'})
  model.province.belongsTo(model.country, {foreignKey: 'country_id'})

  // query to load data from entity
  let find_location = await model.city.findOne({ where: {id: req.body.location}, include: [{ model: model.province,
    include: [{ model: model.country }] }] })

    // create location entity
    let location = await model.location.create({
      id: utils.generateToken(),
      address: req.body.address,
      postal_code: req.body.postalCode,
      country_id: find_location.province.country_id,
      province_id: find_location.province_id,
      city_id: find_location.id
    })

    // create property entity
    let property = await model.property.create({
      type: req.body.type,
      sale_type: req.body.saleType,
      description: req.body.description,
      price: req.body.price,
      bedrooms: req.body.bedrooms,
      bathrooms: req.body.bathrooms,
      characteristics: req.body.characteristics,
      status: req.body.status,
      enabled: true,
      size: req.body.size,
      date_of_creation: Date.now(),
      profile_id: res.user.id,
      location_id: location.id,
    })

    if (req.files.length > 0) {
      // get how many location the renting proposal is about
      for (let i = 0; i < req.files.length; i++) {
        cloudinary.uploader.upload(req.files[i].path, function(result) {
          // create property photo entity
          model.property_photo.create({
            type: result.format,
            size: result.bytes,
            date_of_creation: Date.now(),
            thumbnail_name: result.public_id,
            thumbnail_size: result.bytes,
            name: result.public_id,
            url: result.url,
            content_type: result.resource_type,
            property_id: property.id
          })
        })
      }
    }

  // render data from query
  return successMessage(res, {message : 'the property has been added'})
})


router.put('/property/update',
  permit('property'),
  new validator().property(),
  async function(req, res, next) {

  // init validation req
  const errors = validationResult(req)
  // check if there is errors
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
  }

  // update data to profile model
  console.log(req.body.location)
  /* [err, property] = await to(model.property.update({
    bathrooms: req.body.bathrooms,
    bedrooms: req.body.bedrooms,
    characteristics: req.body.characteristics,
    description: req.body.description,
    enabled: true,
    price: req.body.price,
    sale_type: req.body.saleType,
    size: req.body.size,
    status: req.body.status,
    type: req.body.type,
    location_id: req.body.location
  }, {where: {id: req.body.id}}))
  if(err) throwError('error while updating the property') */

  // render final message
  return successMessage(res, {message : 'the property has been updated'})
})


/* GET properties list. */
router.get('/property/:id', async function(req, res, next) {
  // init limitation row according to page
  model.property.belongsTo(model.location, {foreignKey: 'location_id'})
  model.location.belongsTo(model.country, {foreignKey: 'country_id'})
  model.location.belongsTo(model.province, {foreignKey: 'province_id'})
  model.location.belongsTo(model.city, {foreignKey: 'city_id'})

  // query to load data from entity
  let property = await model.property.findOne({
    include: [ { model: model.location,
      include: [
          {model: model.country},
          {model: model.province},
          {model: model.city}
        ]
    }],
    where: {id: req.params.id}
  })

  res.json({'content': property})
})


/* GET properties list. */
router.get('/properties/list', function(req, res, next) {
  // init limitation row according to page
  let limit = (req.query.size === undefined) ? 8 : req.query.size
  let offset = (req.query.page === undefined) ? 0 : limit * (req.query.page - 1)

  model.property.belongsTo(model.location, {foreignKey: 'location_id'})
  model.location.belongsTo(model.country, {foreignKey: 'country_id'})
  model.location.belongsTo(model.province, {foreignKey: 'province_id'})
  model.location.belongsTo(model.city, {foreignKey: 'city_id'})

  // query to load data from entity
  model.property.findAndCountAll({
    include: [ { model: model.location,
      include: [
          {model: model.country},
          {model: model.province},
          {model: model.city}
        ]
    }],
    order: [
      ['id', 'DESC']
    ],
    limit: limit,
    offset: offset
  }).then(function(entity) {

    // render data from query
    res.json({'content': entity.rows, 'count': entity.count})
  })
})

/**
* @api {GET} /api/p/proposals/list Proposal List api
* @apiName PProposalList
* @apiGroup Api
* @apiSuccess {String} code HTTP status code from API.
* @apiSuccess {String} list proposals.
* @apiSuccess {Integer} count proposals.
*/
router.get('/p/properties/list',
  permit(null, 'ROLE_SELLER','ROLE_LESSOR'),
  async function(req, res, next) {
  // init limitation row according to page
  let limit = (req.query.size === undefined) ? 8 : req.query.size
  let offset = (req.query.page === undefined) ? 0 : limit * (req.query.page - 1)

  model.property.belongsTo(model.location, {foreignKey: 'location_id'})
  model.location.belongsTo(model.country, {foreignKey: 'country_id'})
  model.location.belongsTo(model.province, {foreignKey: 'province_id'})
  model.location.belongsTo(model.city, {foreignKey: 'city_id'})

  // assign default values to filter
  req.query.user_id = res.user.id // to fetch only data from current user
  req.query.status = (req.query.status === undefined) ? 'all' : req.query.status

  // query to load data from entity
  model.property.findAndCountAll({
    where: Query.fetch(req.query),
    include: [ { model: model.location,
      include: [
          {model: model.country},
          {model: model.province},
          {model: model.city}
        ]
    }],
    order: [
      ['id', 'DESC']
    ],
    limit: limit,
    offset: offset
  }).then(function(entity) {

    // render data from query
    res.json({'content': entity.rows, 'count': entity.count})
  })
})




module.exports = router

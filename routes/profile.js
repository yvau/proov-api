var express = require('express')
// let template = require('../services/render')
var router = express.Router()

/* GET buy page. */
router.get('/list', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  // template.render(context, req, res, next)
})

/* GET buy page. */
router.get('/edit/password', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  // template.render(context, req, res, next)
})

/* GET buy page. */
router.get('/edit/contacts', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  // template.render(context, req, res, next)
})

/* GET buy page. */
router.get('/edit/role', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  // template.render(context, req, res, next)
})

/* GET buy page. */
router.get('/edit/information', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  // template.render(context, req, res, next)
})

/* GET buy page. */
router.get('/:id(\\d+)', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  // template.render(context, req, res, next)
})

/* GET buy page. */
router.get('/get_information', function(req, res, next) {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  // template.render(context, req, res, next)
})

module.exports = router

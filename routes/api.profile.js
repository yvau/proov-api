let express = require('express')
let router = express.Router()
let multer  = require('multer')
let utils  = require('../services/utils')
let validator  = require('../validator/ProfileValidator')
let { validationResult } = require('express-validator/check')
let permit = require('../services/auth.policy').permit
let upload = multer()

/* GET profile get information. */
router.get('/profile/get', permit(null), function(req, res, next) {
  return successMessage(res, {message : res.user})
})

/* GET profile get information. */
/* router.post('/profile/get_information', permit(null), function(req, res, next) {
  return successMessage(res, {message : res.user})
}) */

/* POST profile edit information. */
router.post('/profile/get_information',
  upload.array('propertyPhotos[]'),
  // permit(null),
  // new validator().setEditInformation(),
  async function(req, res, next) {
  console.log(req.files)
  const errors = validationResult(req)
  // check if there is errors
  if (!errors.isEmpty()) {
    let paramObject = {'customArray': errors.array(), 'prop': 'param'}
    return errorMessage(res, {messageList: utils.removeDuplicates(paramObject)})
  }
  // update data to profile model
  [err, user] = await to(model.profile_information.update({
    first_name: req.body.firstName,
    last_name: req.body.firstName,
    gender: req.body.gender,
    birth_date: req.body.birthDate
  }, {where: {id: req.user.id}}))

  return successMessage(res, {message : 'the password has been changed'})
})

  module.exports = router

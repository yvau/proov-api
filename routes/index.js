let express = require('express')
// let template = require('../services/render')
let router = express.Router()


/* GET home page. */
/* router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' })
}) */

router.get('/', (req, res, next) => {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/index.vue', data, req.vueOptions)
})

/**
* @api {GET} / Login page
*/
router.get('/about-us/', (req, res, next) => {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/aboutus.vue', data, req.vueOptions)
})

/**
* @api {GET} / Login page
*/
router.get('/blog/', (req, res, next) => {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('blog/index.vue', data, req.vueOptions)
})

/* GET show blog id. */
router.get('/blog/:id', (req, res, next) => {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('blog/show.vue', data, req.vueOptions)
})

/**
* @api {GET} / Login page
*/
router.get('/vone-concept/', (req, res, next) => {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/concept.vue', data, req.vueOptions)
})

/**
* @api {GET} / Login page
*/
router.get('/vone-advantage/', (req, res, next) => {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/advantage.vue', data, req.vueOptions)
})

/**
* @api {GET} / Login page
*/
router.get('/vone-relationship/', (req, res, next) => {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})

/**
* @api {GET} / Login page
*/
router.get('/login', (req, res, next) => {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('auth/login.vue', data, req.vueOptions)
})

/**
* @api {GET} / Register page
*/
router.get('/register', (req, res, next) => {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('auth/register.vue', data, req.vueOptions)
})

/**
* @api {GET} / Register page
*/
router.get('/activate', (req, res, next) => {
  const context = {
    title: 'Vue JS - Server Render',
    meta: `<meta description="vuejs server side render">`
  }
  // template.render(context, req, res, next)
})


module.exports = router

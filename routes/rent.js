var express = require('express')
// let template = require('../services/render')
var router = express.Router()


/* GET buy page. */
router.get('/', function(req, res, next) {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})

/* GET buy page. */
router.get('/overview', function(req, res, next) {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})

/* GET buy page. */
router.get('/new', function(req, res, next) {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})

/* GET rent page. */
router.get('/list', function(req, res, next) {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})

/* GET buy page. */
router.get('/:id(\\d+)/edit', function(req, res, next) {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})

/* GET buy page. */
router.get('/:id(\\d+)', function(req, res, next) {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})

/* GET buy page. */
router.get('/:id(\\d+)/proposition', function(req, res, next) {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})

/* GET buy page. */
router.get('/:id(\\d+)/statistics', function(req, res, next) {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})

/* GET buy page. */
router.get('/:id(\\d+)/settings', function(req, res, next) {
  const data= {
        otherData: 'Something Else'
    };
    req.vueOptions =  {
        head: {
            title: 'Page Title',
            metas: [
                { property:'og:title', content: 'Page Title'},
                { name:'twitter:title', content: 'Page Title'},
            ]
        }
    }
  // template.render(context, req, res, next)
  res.renderVue('home/relationship.vue', data, req.vueOptions)
})


module.exports = router;

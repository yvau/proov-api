/* ============
 * Proposal Transformer
 * ============
 *
 * The transformer for the proposal.
 */

let Query = require('./Query')

module.exports = class QueryProposal extends Query {
  /**
   * Method used to transform a fetched property.
   *
   * @param proposal The fetched property.
   *
   * @returns {Object} The transformed property.
   */
  static fetch (proposal) {
    const object = {}
      if (proposal.status) {
        if (proposal.status !== 'all') {
          object.status = proposal.status
        }
      }
      if (!proposal.status) {
        object.status = 'is_active'
      }
      if (proposal.typeOfProposal) {
        object.type_of_proposal = proposal.typeOfProposal
      }
      if (proposal.user_id) {
        object.profile_id = proposal.user_id
      }
      return object
  }
}

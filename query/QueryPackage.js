/* ============
 * Query Transformer
 * ============
 *
 * The transformer for the PackageProduct.
 */

let Query = require('./Query')

module.exports = class QueryPackage extends Query {
  /**
   * Method used to transform a fetched property.
   *
   * @param proposal The fetched property.
   *
   * @returns {Object} The transformed property.
   */
  static fetch (pack) {
    const object = {}
      if (pack.t) {
        object.type = pack.t
      }
      return object
  }
}

/* ============
 * Proposal Transformer
 * ============
 *
 * The transformer for the property.
 */

let Query = require('./Query')

module.exports = class QueryProperty extends Query {
  /**
   * Method used to transform a fetched property.
   *
   * @param property The fetched property.
   *
   * @returns {Object} The transformed property.
   */
  static fetch (property) {
    const object = {}
      if (property.status) {
        if (property.status !== 'all') {
          object.status = property.status
        }
      }
      if (!property.status) {
        object.status = 'is_active'
      }
      if (property.user_id) {
        object.profile_id = property.user_id
      }
      return object
  }
}

let passport = require('passport')
  , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
let env       = process.env.NODE_ENV || 'development'
let config    = require('../config/index')[env].google

passport.use(new GoogleStrategy({
    clientID: config.clientID,
    clientSecret: config.clientSecret,
    callbackURL: config.callbackURL,
  },
  async function(accessToken, refreshToken, profile, done) {
    [err, user] = await to(model.profile.findOne({ where: { credential: profile._json.email } }))
    if (!user) {
      // query to load increment for profile information data
      model.profile_information.create({
        id: 1000000,
        first_name: profile._json.name,
        last_name: profile._json.name
      }).then(profileInformation => {
        // save data to profile information
        model.profile.create({
          id: 1000000,
          account_non_expired: true,
          account_non_locked: true,
          credential: profile._json.email,
          credentials_non_expired: true,
          date_of_creation: Date.now(),
          date_of_creation_token: Date.now(),
          enabled: true,
        role: 'ROLE_LIMITED',
        profile_information_id: 1000000
        }).then(profile => {
          return done(null, profile)
        })
      })
    }
    if (err) { return done(err) }
    return done(null, user)
  }
))

module.exports = passport

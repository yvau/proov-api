var paypal = require('paypal-rest-sdk')
let env       = process.env.NODE_ENV || 'development'
let config    = require('../config/index')[env].paypal

// configure paypal with the credentials you got when you created your paypal app
paypal.configure({
  'mode': config.mode, //sandbox or live 
  'client_id': config.client_id, // please provide your client id here 
  'client_secret': config.client_secret // provide your client secret here 
})

module.exports = {
  paypal
}
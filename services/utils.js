
let model = require('../models/index')

// remove duplicate errors from checking form
exports.removeDuplicates = (payload) => {
  let customArray = payload.customArray
  let prop = payload.prop
  return customArray.filter((obj, pos, arr) => {
    return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos
  })
}

exports.isValidDate = (payload) => {
  let regEx = /^\d{4}-\d{2}-\d{2}$/
  if(!payload.match(regEx)) return false  // Invalid format
  let d = new Date(payload)
  if(!d.getTime() && d.getTime() !== 0) return false // Invalid date

  return d.toISOString().slice(0,10) === payload
}

exports.isUnderAge = (payload) => {
  let d = new Date(payload)
  let date = new Date()
  date.setFullYear( date.getFullYear() - 18 )
  if (!(d.toISOString().slice(0,10) === payload && date >= d)) return false

  return true
}

// generate token
exports.generateToken = () => {

  // set the length of the string
  let stringLength = 20

  // list containing characters for the random string
  let stringArray = ['0','1','2','3','4','5','6','7','8','9','a','b','c',
  'd','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u',
  'v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M',
  'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','@','-']

  let rndString = ''
  // build a string with random characters
  for (let i = 1; i < stringLength; i++) {
    let rndNum = Math.ceil(Math.random() * stringArray.length) - 1
    rndString = rndString + stringArray[rndNum]
  }

  return rndString
}

let passport = require('passport')
  , LinkedInStrategy = require('passport-linkedin-oauth2').Strategy
let model = require('../models/index')
var env       = process.env.NODE_ENV || 'development'
var config    = require('../config/index')[env].linkedin

passport.use(new LinkedInStrategy({
    clientID: config.clientID,
    clientSecret: config.clientSecret,
    callbackURL: config.callbackURL,
  	scope: ['r_emailaddress', 'r_basicprofile']
  }, function(accessToken, refreshToken, profile, done) {
    // asynchronous verification, for effect...
    process.nextTick(async function () {
    [err, user] = await to(model.profile.findOne({ where: { credential: profile._json.emailAddress } }))
    if (!user) {
      // query to load increment for profile information data
      model.profile_information.create({
        id: 1000400,
        first_name: profile._json.firstName,
        last_name: profile._json.lastName,
        email_contact: profile._json.emailAddress
      }).then(profileInformation => {
        // save data to profile information
        model.profile.create({
          id: 1000400,
          account_non_expired: true,
          account_non_locked: true,
          credential: profile._json.emailAddress,
          credentials_non_expired: true,
          date_of_creation: Date.now(),
          date_of_creation_token: Date.now(),
          enabled: true,
        role: 'ROLE_LIMITED',
        profile_information_id: 1000400
        }).then(profile => {
          user = profile
          return done(null, user)
        })
      })
    }
      // To keep the example simple, the user's LinkedIn profile is returned to
      // represent the logged-in user. In a typical application, you would want
      // to associate the LinkedIn account with a user record in your database,
      // and return that user instead.
      return done(null, user)
  })
  }))

module.exports = passport

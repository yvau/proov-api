let passport = require('passport')
  , FacebookStrategy = require('passport-facebook').Strategy
let model = require('../models/index')
var env       = process.env.NODE_ENV || 'development'
var config    = require('../config/index')[env].facebook

passport.use(new FacebookStrategy({
    clientID: config.clientID,
    clientSecret: config.clientSecret,
    callbackURL: config.callbackURL,
  	profileFields: ['id', 'email', 'displayName', 'photos']
  },
  async function(accessToken, refreshToken, profile, done) {
    [err, user] = await to(model.profile.findOne({ where: { credential: profile._json.email } }))
    if (!user) {
      // query to load increment for profile information data
      model.profile_information.create({
        first_name: profile._json.name,
        last_name: profile._json.name
      }).then(profileInformation => {
        // save data to profile information
        model.profile.create({
          account_non_expired: true,
          account_non_locked: true,
          credential: profile._json.email,
          password: 'facebook',
          credentials_non_expired: true,
          date_of_creation: Date.now(),
          date_of_creation_token: Date.now(),
          enabled: true,
        role: 'ROLE_LIMITED',
        profile_information_id: profileInformation.id
        }).then(profile => {
          return done(null, profile)
        })
      })
    }
    if (err) { return done(err) }
    return done(null, user)
  }
))

module.exports = passport

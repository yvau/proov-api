const fs = require('fs')
const bundle =  require('../server_render/server.bundle.js')

const renderer = require('vue-server-renderer').createRenderer({
  template: require('fs').readFileSync('./views/index.ejs', 'utf-8')
})
exports.render = (context, req, res, next) => {
  return bundle.default({ url: req.url }).then((app) => {
    //context to use as data source
    //in the template for interpolation
    renderer.renderToString(app, context, function (err, html) {
      if (err) {
        if (err.code === 404) {
          res.status(404).end('Page not found')
        } else {
          res.status(500).end('Internal Server Error')
        }
      } else {
        res.end(html)
      }
    })
  }, (err) => {
    console.log(err)
  })
}

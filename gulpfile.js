var gulp = require('gulp')
var sass = require('gulp-sass')
var autoprefixer = require('gulp-autoprefixer')
var browserSync = require('browser-sync')
var plumber = require('gulp-plumber')
var concat = require('gulp-concat')
var nodemon = require('gulp-nodemon')
var cleanCSS = require('gulp-clean-css')
var reload = browserSync.reload
var AUTOPREFIXER_BROWSERS = [
'ie >= 10', 'ie_mob >= 10', 'ff >= 30', 'chrome >= 34', 'Safari >= 7', 'Opera >= 23', 'iOS >= 7', 'ChromeAndroid >= 4.4', 'bb >= 10'
]
var SOURCE = {
  scss: 'assets/scss/*.sass',
  css: 'public/stylesheets'
}
gulp.task('sass', function () {
  return gulp.src(SOURCE.scss)
  .pipe(plumber())
  .pipe(sass().on('error', sass.logError))
  .pipe(cleanCSS())
  .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
  .pipe(concat('style.css'))
  .pipe(gulp.dest(SOURCE.css))
  .pipe(reload({ stream: true }))
})
gulp.task('nodemon', function (cb) {
  return nodemon({
    script: 'index.js',
    ext: 'js'
  }).once('start', cb) // once only get's run........... <drum role>........ once :D
})
gulp.task('browser-sync', function() {
  browserSync({
    proxy: "localhost:3000"
  })
})
gulp.task('default', ['nodemon','sass', 'browser-sync'], function () {
  gulp.watch(SOURCE.scss, ['sass'])
})

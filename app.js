var createError = require('http-errors')
var expressVue = require("express-vue")
var express = require('express')
let favicon = require('serve-favicon')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
require('./global_functions')

var app = express()

// setup favicon
app.use(favicon(__dirname + '/public/favicon.ico'))

const vueOptions = {
    rootPath: path.join(__dirname, 'views'),
    vueVersion: "2.3.4",
    template: {
        html: {
            start: '<!DOCTYPE html><html>',
            end: '</html>'
        },
        body: {
            start: '<body>',
            end: '</body>'
        },
        template: {
            start: '<div id="app">',
            end: '</div>'
        }
    },
    head: {
        title: 'Hello this is a global title',
        scripts: [
            { src: '/static/javascripts/app.34c592b16e9bb173bd46.js' },
        ],
        styles: [
            { style: '/static/stylesheets/app.fa624cea1202bffa336530e670fa1c78.css' }
        ]
    },
    data: {
        foo: true,
        bar: 'yes',
        qux: {
            id: 123,
            baz: 'anything you wish, you can have any kind of object in the data object, it will be global and on every route'
        }
    },
    vue: {

    }
}

const expressVueMiddleware = expressVue.init(vueOptions)
app.use(expressVueMiddleware)

// view engine setup
// app.set('views', path.join(__dirname, 'views'))
// app.set('view engine', 'ejs')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(require('./load.routes.js'))
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error.vue')
})

module.exports = app

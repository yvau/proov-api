/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('proposal_has_view', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'proposal',
        key: 'id'
      }
    },
    profile_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'profile',
        key: 'id'
      }
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'proposal_has_view'
  });
};

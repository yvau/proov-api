/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('location', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    postal_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    country_id: {
      type: DataTypes.CHAR,
      allowNull: false,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    province_id: {
      type: DataTypes.CHAR,
      allowNull: false,
      references: {
        model: 'province',
        key: 'id'
      }
    },
    city_id: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'city',
        key: 'id'
      }
    }
  }, {
    tableName: 'location'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('package_product', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    price_tva: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    count: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    tableName: 'package_product'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('city', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    name_ascii: {
      type: DataTypes.STRING,
      allowNull: true
    },
    alternate_names: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    latitude: {
      type: DataTypes.STRING,
      allowNull: true
    },
    longitude: {
      type: DataTypes.STRING,
      allowNull: true
    },
    f_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    timezone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    province_id: {
      type: DataTypes.CHAR,
      allowNull: false,
      references: {
        model: 'province',
        key: 'id'
      }
    }
  }, {
    tableName: 'city'
  });
};

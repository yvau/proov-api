/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('profile', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    credential: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    date_of_creation_token: {
      type: DataTypes.DATE,
      allowNull: true
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false
    },
    account_non_expired: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    credentials_non_expired: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    account_non_locked: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ip_address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    profile_information_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'profile_information',
        key: 'id'
      }
    },
    payment_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'payment',
        key: 'id'
      }
    }
  }, {
    tableName: 'profile'
  });
};

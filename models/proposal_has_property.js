/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('proposal_has_property', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'proposal',
        key: 'id'
      }
    },
    property_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'property',
        key: 'id'
      }
    },
    payment_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'payment',
        key: 'id'
      }
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_interesting: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    }
  }, {
    tableName: 'proposal_has_property'
  });
};

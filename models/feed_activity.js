/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('feed_activity', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'proposal',
        key: 'id'
      }
    },
    profile_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'profile',
        key: 'id'
      }
    },
    proposal_has_property_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'proposal_has_property',
        key: 'id'
      }
    },
    is_read: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    verb: {
      type: DataTypes.STRING,
      allowNull: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_interesting: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    }
  }, {
    tableName: 'feed_activity'
  });
};

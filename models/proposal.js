/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('proposal', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    bathrooms: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    bedrooms: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    price_minimum: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    price_maximum: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    features: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_furnished: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    type_of_property: {
      type: DataTypes.STRING,
      allowNull: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_urgent: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    size: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING,
      allowNull: true
    },
    age_of_property: {
      type: DataTypes.STRING,
      allowNull: true
    },
    type_of_proposal: {
      type: DataTypes.STRING,
      allowNull: true
    },
    profile_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'profile',
        key: 'id'
      }
    },
    proposal_statistic_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'proposal_statistic',
        key: 'id'
      }
    },
    is_updated: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    }
  }, {
    tableName: 'proposal'
  });
};

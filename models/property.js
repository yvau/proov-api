/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('property', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    sale_type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    bedrooms: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    bathrooms: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    characteristics: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING,
      allowNull: true
    },
    enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    size: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    },
    profile_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'profile',
        key: 'id'
      }
    },
    location_id: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'location',
        key: 'id'
      }
    }
  }, {
    tableName: 'property'
  });
};

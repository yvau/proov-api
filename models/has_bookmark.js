/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('has_bookmark', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    proposal_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'proposal',
        key: 'id'
      }
    },
    property_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'property',
        key: 'id'
      }
    },
    profile_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'profile',
        key: 'id'
      }
    },
    date_of_creation: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'has_bookmark'
  });
};

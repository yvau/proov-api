var express = require( 'express' ),
    router  = express.Router(),
    fs      = require( 'fs' ) ;
// Read each file in the routes directory
fs.readdirSync( './routes' ).forEach( function( route ) {
  // Strip the .js suffix
  let pathfile = route.split( '.' )[ 0 ] ;
  // Ignore index (i.e. this file)
  if ( pathfile === 'index' ) {
    pathfile = '';
  }
  console.log( 'Loading route ' + route + '...' ) ;
  // Mount router
  router.use( '/' + pathfile, require( './routes/' + route ) ) ;
} ) ;
module.exports = router ;
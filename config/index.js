let config = {
  "development": {
    "jwt_secret": "secret",
    "database": {
      "username": "postgres",
      "password": "postgres",
      "database": "proov",
      "host": "127.0.0.1",
			"dialect": "postgres",
			"define": {
				"timestamps": false
			}
		},
		"paypal": {
			"mode": "sandbox",
      "client_id": "ASSljN1X2fhtOalonExcpolVsWmMWGVBI-NiTSiQpkksE02CFoenGaBlT13DRxlJxG03g5UuKTuScwpj",
      "client_secret": "EDJ8tBVgcQthBJBA6odKasLX0kaOQuzQMCLB7kdembe-GRuPZiv48SzrIrvsrWaCXX2mKQATGPyFC28b"
		},
		"facebook": {
			"clientID": "411906505976854",
			"clientSecret": "437b43c8ccd8d888a807727cf074c692",
			"callbackURL": "http://localhost:9000/auth/facebook/callback"
		},
		"twitter": {
			"consumerKey": "dy4cDwsM0PY5erbpT40ojXJj1",
      "consumerSecret": "AwSwVpWlAT3g7d0Rm9z2CbLdbIMElWU66qVbnqQxTldvDGgdNz",
      "callbackURL": "http://127.0.0.1:9000/auth/twitter/callback"
		},
		"google": {
			"clientID": '1032982174953-v9efbtg1s9sd6f5tgngcp53dgr17ng25.apps.googleusercontent.com',
      "clientSecret": 'VhTkGkLFcs8aBxWo9SjxG7mb',
      "callbackURL": "http://127.0.0.1:9000/auth/google/callback"
		},
		"linkedin": {
			"clientID": "77kd0sxqg0qptv",
      "clientSecret": "CqvwdIola0gAm7m0",
      "callbackURL": "http://127.0.0.1:9000/auth/linkedin/callback"
		},
		"mail": {
		  "smtp": "localhost",
		  "port": "2500",
			"secure": false,
			"tls":"true",
		  "auth": {
				"user": "test@test.Com",
				"pass": ""
			},
			"url": "http://localhost:3000/activate"
	  }
  },
  "production": {
    "jwt_secret": "MiOiJsb3JhLWFwcC1zZXJ2ZXIiLCJhdWQiOiJsb3JhLWFwcC",
	  "database": {
	    "username": "postgres",
	    "password": "postgres",
	    "database": "proov",
	    "host": "139.59.72.244",
		  "dialect": "postgres",
			"define": {
			  "timestamps": false
			}
		},
		"paypal": {
      "mode": "sandbox",
      "client_id": "ASSljN1X2fhtOalonExcpolVsWmMWGVBI-NiTSiQpkksE02CFoenGaBlT13DRxlJxG03g5UuKTuScwpj",
      "client_secret": "EDJ8tBVgcQthBJBA6odKasLX0kaOQuzQMCLB7kdembe-GRuPZiv48SzrIrvsrWaCXX2mKQATGPyFC28b"
		},
		"facebook": {
			"clientID": "411906505976854",
			"clientSecret": "437b43c8ccd8d888a807727cf074c692",
			"callbackURL": "http://139.59.72.244:9000/auth/facebook/callback"
		},
		"twitter": {
			"consumerKey": "dy4cDwsM0PY5erbpT40ojXJj1",
      "consumerSecret": "AwSwVpWlAT3g7d0Rm9z2CbLdbIMElWU66qVbnqQxTldvDGgdNz",
      "callbackURL": "http://139.59.72.244:9000/auth/twitter/callback"
		},
		"google": {
			"clientID": '1032982174953-v9efbtg1s9sd6f5tgngcp53dgr17ng25.apps.googleusercontent.com',
      "clientSecret": 'VhTkGkLFcs8aBxWo9SjxG7mb',
      "callbackURL": "http://139.59.72.244:9000/auth/google/callback"
		},
		"linkedin": {
			"clientID": "77kd0sxqg0qptv",
      "clientSecret": "CqvwdIola0gAm7m0",
      "callbackURL": "http://139.59.72.244:9000/auth/linkedin/callback"
		},
	  "mail": {
		  "smtp": "smtp.gmail.com",
		  "port": "465",
			"secure": true,
			"tls":"false",
		  "auth": {
				"user": "mobiot.fabrice@gmail.com",
				"pass": "@@@Abcdefgh000"
			},
			"url": "http://139.59.72.244:9000/activate"
	  }
  }
}

module.exports = config;
